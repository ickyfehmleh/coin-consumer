import React, {Component} from 'react';
import './App.css';

class App extends Component {
  state = {
    coinDenominations: {},
    errorMessage:'',
    hasResults: false,
    errorCount: 0
  }

  setErrorMessage(msg) {
    let errorCount = this.state.errorCount+1;

    if(errorCount >= 2) {
      if(errorCount === 4) {
        msg = msg + " LAST WARNING!!1";
      } else {
        msg = msg + " You have been warned!";
      }
    }
    this.setState({errorMessage:msg, coinDenominations:{}, hasResults: false, errorCount: errorCount});
  }

  setCoins(v) {
    this.setState({ errorMessage:'', coinDenominations:v, hasResults:true});
  }

  handleFormSubmit = async(e) => {
    e.preventDefault();
    const dollas = e.target.elements.amount.value;

    if(isNaN(dollas)) {
      this.setErrorMessage("Numbers only my dude.");
    } else if(dollas === '') {
      this.setErrorMessage("Only monsters submit a forms without providing a value.");
    } else {
      const apiResults = await fetch(`http://localhost:8080/convert?amount=${dollas}`);

      if(apiResults.status === 200) {
        const jaysonsData = await apiResults.json();
        this.setCoins(jaysonsData);
      } else {
        this.setErrorMessage("Something went wrong, please try again");
      }
    }
  }

  render() {
    const coins = this.state.coinDenominations;
    const errorMessage = this.state.errorMessage;
    const hasResults = this.state.hasResults;

    if(this.state.errorCount >= this.TOLERANCE) {
      // render(), for some reason, doesnt like to have the body returned as a string:
      // return(atob('PG1ldGEgaHR0cC1lcXVpdj0icmVmcmVzaCIgY29udGVudD0iMDtVUkw9J2h0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9ZFF3NHc5V2dYY1EnIi8+'));
      // so i cant be sly
      return(<meta http-equiv="refresh" content="0;URL='https://www.youtube.com/watch?v=dQw4w9WgXcQ'"/>);
    }

    return (
        <div className="App">
          <h1 className="App-header">Dolla Bills, Y'all</h1>
          <form onSubmit={this.handleFormSubmit}>
            How much you got?: <input type="text" name="amount" id="amount"/>
            <button type="submit">Send It</button>
          </form>

          <div className={hasResults ? 'shownItem' : 'hiddenItem'}>
            <br/>
            <CoinDisplay coin="Silver Dollas" value={coins.silverDollars} />
            <CoinDisplay coin="Half Dollas" value={coins.halfDollars}/>
            <CoinDisplay coin="Quarters" value={coins.quarters}/>
            <CoinDisplay coin="Dimes" value={coins.dimes}/>
            <CoinDisplay coin="Nickels" value={coins.nickels}/>
            <CoinDisplay coin="Pennies" value={coins.pennies}/>
          </div>

          <p className={errorMessage === '' ? 'hiddenItem' : 'shownItem'}>
            <b>ERROR</b>: <i>{errorMessage}</i>
          </p>
        </div>
    );
  }

  TOLERANCE = 5;
}

const CoinDisplay = ({coin,value}) =>
    <div>
      <b>{coin}</b>: <i>{value > 0 ? value : 'none'}</i><br/>
    </div>;

export default App;
